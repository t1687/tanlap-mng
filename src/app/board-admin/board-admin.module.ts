import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BoardAdminRoutingModule } from './board-admin-routing.module';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { ProductManagerComponent } from './product-manager/product-manager.component';
import { SalesManagerComponent } from './sales-manager/sales-manager.component';
import {
  BreadcrumbModule, ButtonsModule,
  ChartsModule,
  IconsModule,
  InputsModule,
  MDBBootstrapModule,
  TableModule,
  WavesModule
} from 'angular-bootstrap-md';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [
    DashboardComponent,
    UserManagerComponent,
    ProductManagerComponent,
    SalesManagerComponent
  ],
  imports: [
    CommonModule,
    BoardAdminRoutingModule,
    ChartsModule,
    BreadcrumbModule,
    FormsModule,
    TableModule,
    InputsModule,
    IconsModule,
    WavesModule,
    ButtonsModule
  ]
})
export class BoardAdminModule { }
