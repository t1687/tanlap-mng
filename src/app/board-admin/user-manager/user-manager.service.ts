import { Injectable } from '@angular/core';
import {TokenStorageService} from '../../_services/token-storage.service';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Product} from '../product-manager/product';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserManagerService extends TokenStorageService {


  apiUrl = environment.baseUrl;

  constructor(http: HttpClient,
              router: Router) {
    super(http, router);
  }

  getUser(): Observable<any> {
    return super.doGet('/api/user/all');
  }
  delete(id: any): Observable<any> {
    return super.doDelete('/api/user/delete/'+id);
  }

}
