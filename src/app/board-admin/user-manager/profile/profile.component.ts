import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../_services/auth.service';
import { TokenStorageService } from '../../../_services/token-storage.service';
import { ProfileService } from './profile.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  currentUser: any;
  sourceImg:any;
  constructor(private profileService: ProfileService,
              private route: ActivatedRoute,
              private router: Router) {
    this.currentUser = {
      avt: null,
      username: '',
      email: '',
      firstName:'',
      lastName:'',
      phone:'',
      address:'',
      country:'',
      password: ''
    }
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe( paramMap => {
      const id = paramMap.get('id');
      if(!id) {
        return;
      }
      this.profileService.getProfile(id).subscribe(res => {
        this.currentUser = res
        console.log(res)
      })
    }, () => {

    });
  }
  onFileChange(event:any) {


    console.log();
    let file = event.target.files[0];
    this.getBase64(file).then(img => this.currentUser.avt = img)

    }
  getBase64(file:any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result?.toString());
      reader.onerror = error => reject(error);
    });
  }

  save() {
    if(!this.currentUser.id) {
      this.profileService.createUser(this.currentUser).subscribe(res => {
        alert("Thêm mới thành công");
        this.router.navigate(['/admin/user'])
      },err=> alert("Đã có lỗi xảy ra"));
    } else  {
      this.profileService.saveUser(this.currentUser).subscribe(res => {
        alert("Cập nhật thành công");
        this.router.navigate(['/admin/user'])
      },err=> alert("Đã có lỗi xảy ra"));
    }

  }
}
