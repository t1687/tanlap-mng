import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../../../_services/token-storage.service';
import {Router} from '@angular/router';
import {Product} from '../../product-manager/product';
@Injectable({
  providedIn: 'root'
})
export class ProfileService extends TokenStorageService {


  constructor(http: HttpClient,
              router: Router) {
    super(http, router);
  }

  getUser(): Observable<any> {
    return super.doGet('/api/user/all');
  }

  getProfile(id:any): Observable<any>  {
    return super.doGet('/api/user/profile/'+id);
  }
  createUser(user: any): Observable<any> {
    return super.doPost('/api/auth/signup', user);
  }
  saveUser(user: any): Observable<any> {
    return super.doPost('/api/user/update', user);
  }



}
