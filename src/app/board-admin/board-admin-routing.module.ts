import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {BoardAdminComponent} from './board-admin.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuard} from '../_services/auth.gaurd';
import {UserManagerComponent} from './user-manager/user-manager.component';
import {SalesManagerComponent} from './sales-manager/sales-manager.component';
import {ProfileComponent} from './user-manager/profile/profile.component';

const routes: Routes = [

  { path: '', canActivate: [AuthGuard], component: BoardAdminComponent,

    children: [
      { path: '', redirectTo: 'dashboard'},
      { path: 'dashboard', canActivate: [AuthGuard], component: DashboardComponent},
      { path: 'product', canActivate: [AuthGuard],
        loadChildren: () => import('./product-manager/product-manager.module').then(m => m.ProductManagerModule)},
      { path: 'user', canActivate: [AuthGuard], component: UserManagerComponent },
      { path: 'user/create', canActivate: [AuthGuard], component: ProfileComponent },
      { path: 'user/update/:id', canActivate: [AuthGuard], component: ProfileComponent },
      { path: 'sales', canActivate: [AuthGuard], component: SalesManagerComponent },
    ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardAdminRoutingModule { }
