import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {CommonService} from '../../_services/common.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Product} from './product';
import { Router} from '@angular/router';
import {TokenStorageService} from '../../_services/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends TokenStorageService{

  apiUrl = environment.baseUrl;

  constructor(http: HttpClient,
              router: Router) {
    super(http, router);
  }

  saveProduct(product: Product): Observable<any> {
    return super.doPost('/api/product/save', product);
  }

  getProduct(id: any): Observable<any> {
    return super.doGet('/api/product/get/' + id);
  }

  getAll(): Observable<any> {
    return super.doGet('/api/product/all');
  }
  delete(id: any): Observable<any> {
    return super.doDelete('/api/product/delete/'+id);
  }

  getServiceName(): string {
    return 'ProductService';
  }
}
