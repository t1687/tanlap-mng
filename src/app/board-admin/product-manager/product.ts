export class Product {
  id?: number;
  serial?: string;
  name?: string;
  kind?: string;
  description?: string;
  avatar?: string;
  msd?: string;
  price?: string;
  number?: string;
  dvt?: string;
}
