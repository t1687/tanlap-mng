import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {ProductService} from '../product.service';
import {ActivatedRoute, Route, Router} from '@angular/router';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {

  productForm: FormGroup;

  isSelect = true;
  isChangeImage = true;
  filesImage: any;
  fileName: any;
  isChangeDoc = true;
  filesDoc: string | any[] = [];
  previewUrl: any;
  constructor(private sanitizer: DomSanitizer,
              private fb: FormBuilder,
              private productService: ProductService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit(): void {
    this.loading = true;
    this.route.paramMap.subscribe( paramMap => {
      const id = paramMap.get('id');

      this.productService.getProduct(id).subscribe(product => {
        this.loading = false
        this.productForm = this.fb.group({
          id: [product.id],
          serial: [product.serial],
          name: [product.name],
          kind: [product.kind],
          description: [product.description],
          avatar: [product.avatar],
          msd: [product.msd],
          price: [product.price],
          number: [product.number],
          dvt: [product.dvt],
        });
        this.isSelect = false;
      }, () => this.loading= false)
    })
  }
  config = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: 'h1',
      },
    ]
  };
  loading = false;

  doChangeImage(files: any) {
    this.isChangeImage = true;
    this.filesImage = files;
    this.previewFile(files);
    this.getBase64(this.filesImage[0]).then(
      data => {
        this.productForm.get('avatar')?.setValue(data);
      });
  }

  onFileChange(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      const files = event.target.files;
      this.clearImage();
      this.doChangeImage(files);

    }
  }
  getBase64(file:any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result?.toString());
      reader.onerror = error => reject(error);
    });
  }
  doClearDoc() {
    this.isChangeDoc = true;
    this.filesDoc = [];

  }

  previewFile(files: any[]) {
    const objUrl = window.URL.createObjectURL(files[0]);
    this.previewUrl = this.sanitizer.bypassSecurityTrustUrl(objUrl);
    this.isSelect = false;
  }

  doClearImage() {
    this.isChangeImage = true;
    this.filesImage = [];

  }
  clearImage() {
    this.isSelect = true;
    this.doClearImage();
    this.productForm.get('avatar')?.setValue(null)
  }


  submit() {
    console.log(this.productForm.value);
    this.productService.saveProduct(this.productForm.value).subscribe(res => {
        this.router.navigate(['admin/product'])
      },
      error => {
        console.log(error)
      }
    )
  }

}
