import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BoardAdminComponent} from '../board-admin.component';
import {AuthGuard} from '../../_services/auth.gaurd';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {ProductManagerComponent} from './product-manager.component';
import {UserManagerComponent} from '../user-manager/user-manager.component';
import {SalesManagerComponent} from '../sales-manager/sales-manager.component';
import { ProductCreateComponent } from './product-create/product-create.component';
import {BreadcrumbModule, ButtonsModule, CardsModule, WavesModule} from 'angular-bootstrap-md';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ProductUpdateComponent } from './product-update/product-update.component';

const routes: Routes = [
  { path: '', component: ProductManagerComponent},
  { path: 'create', canActivate: [AuthGuard], component: ProductCreateComponent},
  { path: 'update/:id', canActivate: [AuthGuard], component: ProductUpdateComponent}
];

@NgModule({
  declarations: [
    ProductCreateComponent,
    ProductUpdateComponent
  ],
  imports: [
    CommonModule,
    AngularEditorModule,
    RouterModule.forChild(routes),
    BreadcrumbModule,
    CardsModule,
    ButtonsModule,
    WavesModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule]
})
export class ProductManagerModule { }
