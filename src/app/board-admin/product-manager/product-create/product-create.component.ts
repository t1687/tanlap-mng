import {Component, ElementRef, EventEmitter, HostListener, OnInit, ViewChild} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ProductService} from '../product.service';
import {Router} from '@angular/router';
import { ToastService } from 'src/app/public/toast/toast.service';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {

  productForm: FormGroup;


  constructor(private sanitizer: DomSanitizer,
              private fb: FormBuilder,
              private productService: ProductService,
              private router: Router,) {

  }

  ngOnInit(): void {
    this.productForm = this.fb.group({
      id: [null],
      serial: [null],
      name: [null],
      kind: [null],
      description: [null],
      avatar: [null],
      msd: [null],
      price: [null],
      number: [null],
      dvt: [null],
    });
  }
  config = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  isSelect = true;
  isChangeImage = true;
  filesImage: any;
  fileName: any;
  isChangeDoc = true;
  filesDoc: string | any[] = [];
  previewUrl: any;
  doChangeImage(files: any) {
    this.isChangeImage = true;
    this.filesImage = files;
    this.previewFile(files);
    this.getBase64(this.filesImage[0]).then(
      data => {
        this.productForm.get('avatar')?.setValue(data);
      });
  }

  onFileChange(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      const files = event.target.files;
      this.clearImage();
      this.doChangeImage(files);

    }
  }
  getBase64(file:any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result?.toString());
      reader.onerror = error => reject(error);
    });
  }
  doClearDoc() {
    this.isChangeDoc = true;
    this.filesDoc = [];

  }

  previewFile(files: any[]) {
    const objUrl = window.URL.createObjectURL(files[0]);
    this.previewUrl = this.sanitizer.bypassSecurityTrustUrl(objUrl);
    this.isSelect = false;
  }

  doClearImage() {
    this.isChangeImage = true;
    this.filesImage = [];

  }
  clearImage() {
    this.isSelect = true;
    this.doClearImage();
    this.productForm.get('avatar')?.setValue(null)
  }


  submit() {
    console.log(this.productForm.value);
    this.productService.saveProduct(this.productForm.value).subscribe(res => {
      this.router.navigate(['admin/product'])
      },
      error => {
      console.log(error)
    }
    )
  }
}
