import {ErrorHandler, Injectable, NgZone} from '@angular/core';
import {AuthService} from '../_services/auth.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {ApiErrorArgsInvalid, ApiErrorForbidden, ApiErrorNotFound, ApiErrorResponse, ApiErrorTokenInvalid} from './error-response';
import {TokenStorageService} from '../_services/token-storage.service';

@Injectable()
export class CustomErrorHandle implements ErrorHandler {
  // @ts-ignore
  constructor(
    private ngzone: NgZone,
    private auth: AuthService,
    private router: Router,
    private tokenService: TokenStorageService
  ) {
  }
  // tslint:disable-next-line:typedef
  getHttpStatusCode(error: any) {
    if (!navigator.onLine) {
      return 'noInternet';
    }
    return error.status;
  }
  handleError(err: any): void {
    switch (err.constructor) {
      case HttpErrorResponse: {
        console.log(this.getHttpStatusCode(err));
        break;
      }
      case ApiErrorArgsInvalid: {
        if (err.params) {
          let msg = '';
          Object.keys(err.params).forEach((key) => {
            msg += err.params[key] + '<br>';
          });
          this.showDialog(
            err.code,
            err.msg
          );
        }
        break;
      }
      case ApiErrorResponse: {
        this.showDialog(
          err.code,
          err.msg
        );
        break;
      }
      case ApiErrorTokenInvalid: {
        // if (!this.auth.isAuthed()) {
        this.tokenService.signOut();
        this.ngzone.run(() => {
          // bypass form leave guard with queryParams expired is 1
          this.router.navigate(['login'], { queryParams: { expired: '1' } });
        });
        // }
        break;
      }
      case ApiErrorForbidden: {
        this.ngzone.run(() => {
          this.router.navigate(['public', 'access-denied']);
        });
        break;
      }
      case ApiErrorNotFound: {
        this.ngzone.run(() => {
          this.router.navigate(['public', 'not-found']);
        });
        break;
      }
      default: {
        if (err.name === 'TimeoutError') {
          this.showDialog(
            '',
            'Time out'
          );
          break;
        }
      }
    }
  }

  showDialog(title: string, msg: string): void {
    this.ngzone.run(() => {
      confirm(title + msg);
    });
  }
}
