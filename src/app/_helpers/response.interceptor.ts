import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {ApiErrorArgsInvalid, ApiErrorForbidden, ApiErrorNotFound, ApiErrorResponse, ApiErrorTokenInvalid} from './error-response';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  private readonly BASE_URL = environment.baseUrl;
  private readonly IGNORE_URLS = ['/assets/i18n'];
  private readonly NOT_FOUND_WILL_THROW = [];
  private readonly CLIENT_LOG_API = '/common/log';
  constructor() {}

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    let httpStatus = 'SUCCESS';
    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => {
        this.handleResponse(event, req);
      }, (err: any) => {
        throw err;
      }),
      catchError((err: any) => {
        httpStatus = 'FAIL';
        const error = this.handleError(err, req);
        return throwError(error);
      })
    );
  }
  private isRequestToApi(requestUrl: string): boolean {
    return requestUrl.startsWith(this.BASE_URL);

  }

  handleResponse(event: any, req: HttpRequest<any>): void {
    if (!(event instanceof HttpResponse)) {
      return;
    }
    const requestUrl = req.url;
    if (!this.isRequestToApi(requestUrl)) {
      return;
    }
    if (this.IGNORE_URLS.find(url => requestUrl.includes(url))) {
      return;
    }
    const { isParseJsonError, response } = this.safeParseJson(event.body);

    if (isParseJsonError) {
      throw new ApiErrorResponse(500, 'Cant parse object to json');
    } else {
      if (!event.status) {
        throw new ApiErrorResponse(500, 'Cant get response code');
      }
      const rsCode = event.status;
      if (this.isResponseNotFound(event.status)
        && this.NOT_FOUND_WILL_THROW.find(url => requestUrl.includes(url))
      ) {
        throw new ApiErrorNotFound();
      }
      if (!this.isResponseSuccess(event.status)) {
        throw new ApiErrorResponse(rsCode, response);
      }
    }
  }

  handleError(err: any, req: HttpRequest<any>): void {
    const requestUrl = req.url;
    switch (err.constructor) {
      case HttpErrorResponse: {
        if (requestUrl.includes(this.CLIENT_LOG_API)) {
          break;
        }
        if (err.status === 401) {
          err = new ApiErrorTokenInvalid(401, 'Token expired or invalid');
        } else if (err.status === 403) {
          err = new ApiErrorForbidden(403, 'Not permission to access resource');
        } else if (err.status === 400) {
          err = new ApiErrorArgsInvalid(400, 'Request invalid', err.error);
        }
        break;
      }
      default: {
        of(err).pipe(
          tap(() => this.sendLog(req, err))
        );
        break;
      }
    }
    return err;
  }
  sendLog(req: HttpRequest<any>, err: any): void {
    // TODO: send log to server
  }
  private safeParseJson(raw: any): { isParseJsonError: boolean, response: any } {
    if (typeof raw === 'object') {
      return { isParseJsonError: false, response: raw };
    } else {
      const response = JSON.parse(raw);
      return { isParseJsonError: this.isError(response), response };
    }
  }
  isError(value: any): value is Error;

  // @ts-ignore
  private isResponseSuccess(result: any): boolean {
    return result === 200 || result === 201;
  }

  private isResponseNotFound(result: any): boolean {
    return result === 201;
  }

}
interface Error {
  name: string;
  message: string;
  stack?: string;
}
