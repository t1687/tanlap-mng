import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import {RouterModule, Routes} from '@angular/router';
import {ButtonsModule} from 'angular-bootstrap-md';


const routes: Routes = [
  {
    path: 'not-found',
    component: NotFoundComponent
  }
  ];

@NgModule({
  declarations: [
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ButtonsModule
  ],
  exports:[RouterModule]
})
export class PublicModule { }
