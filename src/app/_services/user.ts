export class User {
    public id!: number
	public username!: string
	public email!: string
	
	public  firstName!: string
	public  lastName!: string
	public  address!: string
	public  phone!: string
	public  country!: string
	public  avt!: string
}