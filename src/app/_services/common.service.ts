import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export abstract class CommonService {

  private baseUrl = '';
  private basePath = '';

  constructor() {
    this.baseUrl = environment.baseUrl;
  }

  abstract getHttp(): HttpClient;

  abstract getServiceName(): string;

  protected doGet(url: string, httpParams?: HttpParams, httpHeaders?: HttpHeaders): Observable<any> {
    const requestUrl = `${this.baseUrl}${this.basePath}${url}`;
    return this.getHttp().get<any>(requestUrl, { headers: httpHeaders, params: httpParams });
  }

  protected doPost(url: string, body: any, httpParams?: HttpParams, httpHeaders?: HttpHeaders): Observable<any> {
    const requestUrl = `${this.baseUrl}${this.basePath}${url}`;
    return this.getHttp().post<any>(requestUrl, body || {}, {
      headers: httpHeaders,
      params: httpParams
    });
  }

  protected postDataBlob(url: string, body: any, header?: HttpHeaders, inputParams?: HttpParams): Observable<any> {
    const requestUrl = `${this.baseUrl}${this.basePath}${url}`;
    return this.getHttp().post<any>(requestUrl, body, { headers: header, params: inputParams, responseType: 'blob' as 'json' });
  }

  protected doDelete(url: string, httpParams?: HttpParams, httpHeaders?: HttpHeaders) {
    const requestUrl = `${this.baseUrl}${this.basePath}${url}`;
    return this.getHttp().delete<any>(requestUrl, { headers: httpHeaders, params: httpParams });
  }

  public logDebug(value: any) {
    if (!environment.production) {
      console.log(`${this.getServiceName()} [Debug]:`, value);
    }
  }


}
