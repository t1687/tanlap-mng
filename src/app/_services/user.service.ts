import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorageService } from './token-storage.service';
import { Router } from '@angular/router';

const API_URL = '/api/test/';

@Injectable({
  providedIn: 'root'
})
export class UserService extends TokenStorageService {
  constructor(http: HttpClient,
              router: Router
    ) {
        super(http, router);
   }

  getPublicContent(): Observable<any> {
    return super.doGet(API_URL + 'all', );
  }

  getUserBoard(): Observable<any> {
    return super.doGet(API_URL + 'user', );
  }

  getModeratorBoard(): Observable<any> {
    return super.doGet(API_URL + 'mod', );
  }

  getAdminBoard(): Observable<any> {
    return super.doGet(API_URL + 'admin', );
  }
  getProfile(): Observable<any> {
    return super.doGet(API_URL + 'profile');
  }
}
