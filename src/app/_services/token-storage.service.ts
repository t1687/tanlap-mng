import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {


  private baseUrl = '';
  private basePath = '';



  constructor(
               private http: HttpClient,
               private router: Router) {
    this.baseUrl = environment.baseUrl;

  }
  getHttp(): HttpClient {
    return this.http;
  }

  signOut(): void {
    window.localStorage.clear();
  }

  public saveToken(token: string): void {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return window.localStorage.getItem(TOKEN_KEY);
  }

  public createUser(user: any): void {
    window.localStorage.removeItem(USER_KEY);
    window.localStorage.setItem(USER_KEY, JSON.stringify(user));
    window.localStorage.setItem("is_authen", "true");
  }

  public getUser(): any {
    const user = window.localStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }


  protected doGet(url: string, httpParams?: HttpParams, httpHeaders?: HttpHeaders): Observable<any> {
    const requestUrl = `${this.baseUrl}${this.basePath}${url}`;
    return this.getHttp().get<any>(requestUrl, { headers: httpHeaders, params: httpParams });
  }

  protected doPost(url: string, body: any, httpParams?: HttpParams, httpHeaders?: HttpHeaders): Observable<any> {
    const requestUrl = `${this.baseUrl}${this.basePath}${url}`;
    return this.getHttp().post<any>(requestUrl, body || {}, {
      headers: httpHeaders,
      params: httpParams
    });
  }

  protected postDataBlob(url: string, body: any, header?: HttpHeaders, inputParams?: HttpParams): Observable<any> {
    const requestUrl = `${this.baseUrl}${this.basePath}${url}`;
    return this.getHttp().post<any>(requestUrl, body, { headers: header, params: inputParams, responseType: 'blob' as 'json' });
  }

  protected doDelete(url: string, httpParams?: HttpParams, httpHeaders?: HttpHeaders) {
    const requestUrl = `${this.baseUrl}${this.basePath}${url}`;
    return this.getHttp().delete<any>(requestUrl, { headers: httpHeaders, params: httpParams });
  }


  protected handleError(error: any): Promise<any> {

    debugger
      // if stauts error is 403 (Access Denied), re
      if(error.status >400){
          this.router.navigate(['/login']);
      }
      return Promise.reject(error);
    }
}
